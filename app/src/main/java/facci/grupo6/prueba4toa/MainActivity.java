package facci.grupo6.prueba4toa;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import facci.grupo6.prueba4toa.utilidades.UtilidadesEstudiantes;

public class MainActivity extends AppCompatActivity {

    EditText ETnombre,ETapellido,ETfacultad,ETciudad,ETfecha;

    Button btningresar,btnconsultar;
    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conn = new ConexionSQLiteHelper(this,"bd estudiantes",null,1);
        ETnombre= (EditText)findViewById(R.id.ETnombre);
        ETapellido = (EditText)findViewById(R.id.ETapellidos);
        ETfacultad=(EditText)findViewById(R.id.ETfacultad);
        ETciudad=(EditText)findViewById(R.id.ETciudad);
        ETfecha = (EditText) findViewById(R.id.ETfecha);
        btningresar=(Button)findViewById(R.id.btningresar);
        btnconsultar=(Button)findViewById(R.id.btnconsultar);

        btningresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarEstudiante();
            }
        });
    }

    private void registrarEstudiante() {

        SQLiteDatabase db = conn.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UtilidadesEstudiantes.CAMPO_NOMBRE,ETnombre.getText().toString());
        values.put(UtilidadesEstudiantes.CAMPO_APELLIDO,ETapellido.getText().toString());
        values.put(UtilidadesEstudiantes.CAMPO_FACULTAD,ETfacultad.getText().toString());
        values.put(UtilidadesEstudiantes.CAMPO_CIUDAD,ETciudad.getText().toString());
        values.put(UtilidadesEstudiantes.CAMPO_CIUDAD,ETciudad.getText().toString());
        Long NombreResultante= db.insert(UtilidadesEstudiantes.TABLA_ESTUDIANTE,UtilidadesEstudiantes.CAMPO_NOMBRE,values);
        Toast.makeText(getApplicationContext(),"Registro id:"+NombreResultante,Toast.LENGTH_LONG).show();
        db.close();


    }


    }

