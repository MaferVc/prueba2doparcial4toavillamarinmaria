package facci.grupo6.prueba4toa.entidades;

public class Estudiantes {
    private Integer fecha;
    private String nombre;
    private String apellido;
    private String facultad;
    private String ciudad;


    public Estudiantes (Integer fecha, String nombre, String apellido, String facultad, String ciudad) {
        this.fecha = fecha;
        this.nombre = nombre;
        this.apellido = apellido;
        this.facultad = facultad;
        this.ciudad = ciudad;
    }

    public Integer getId() {
        return fecha;
    }

    public void setId(Integer fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) { this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) { this.apellido = apellido;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String carrera) {
        this.facultad = carrera;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
