package facci.grupo6.prueba4toa.utilidades;

public class UtilidadesEstudiantes {
    public static  final String TABLA_ESTUDIANTE ="estudiante";
    public static  final String CAMPO_NOMBRE ="nombre";
    public static  final String CAMPO_APELLIDO ="apellido";
    public static  final String CAMPO_FACULTAD ="facultad";
    public static  final String CAMPO_CIUDAD ="ciudad";
    public static  final String CAMPO_FECHA ="fecha";

    public static  final String CREAR_TABLA_ESTUDIANTE ="CREATE TABLE "+
            TABLA_ESTUDIANTE+" ("+
            CAMPO_NOMBRE+" INTEGER, "+
            CAMPO_NOMBRE+" TEXT, "+
            CAMPO_FACULTAD+" TEXT, "+
            CAMPO_CIUDAD+" TEXT, "+
            CAMPO_FECHA+" TEXT)";
}
